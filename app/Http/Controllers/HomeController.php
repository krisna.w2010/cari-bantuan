<?php

namespace App\Http\Controllers;

use App\Models\Assistance\Assistance;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $type = $request->type ?? $request->type;
        $this->addParam('type', $type);

        $province = $request->province ?? $request->province;
        $regencies = [];
        if ($province) {
            $regencies = $this->getProvinces()[$province];
        }
        $this->addParam('regencies', $regencies);
        $this->addParam('province', $province);

        $regency = $request->regency ?? $request->regency;
        $this->addParam('regency', $regency);

        $assistances = Assistance::orderBy('created_at', 'DESC')
            ->when($type != '', function ($q) use ($type) {
                return $q->where('type', $type);
            })
            ->when($province != '', function ($q) use ($province) {
                return $q->where('province', $province);
            })
            ->when($regency != '', function ($q) use ($regency) {
                return $q->where('regency', $regency);
            })
            ->paginate(10);

        $this->addParam('assistances', $assistances);
        $this->addParam('items', $this->getProvinces());
        return view('home', $this->viewParam);
    }
}
