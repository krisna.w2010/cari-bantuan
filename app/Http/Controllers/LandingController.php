<?php

namespace App\Http\Controllers;

use App\Models\Assistance\Assistance;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function store(Request $request)
    {
        Assistance::create($request->all());
        return redirect()->route('thank')->with('success', 'Success');
    }

    public function thank()
    {
        return view('thank');
    }
}
