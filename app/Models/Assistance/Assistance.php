<?php

namespace App\Models\Assistance;

use Illuminate\Database\Eloquent\Model;

class Assistance extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'assistances';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'type', 'address', 'province', 'regency', 'district', 'village', 'lat', 'lng', 'mobile_phone', 'needs'];
}
