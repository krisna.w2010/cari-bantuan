<?php

namespace App\Helpers;

class Helper
{
    public function getUserType($type)
    {
        if ($type == 'R') {
            return 'Penerima';
        }
        return 'Pemberi';
    }
}
