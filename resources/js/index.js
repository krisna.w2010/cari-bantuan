$(function () {
    $('#province').change(function () {
        let regencies = $(this).find(':selected').attr('data-regency');
        regencies = regencies.split(',');

        const regency = $(`#regency`);
        regency.find('option').remove().end().removeAttr("disabled");
        for (var i = 0; i < regencies.length; i++) {
            regency.append(`<option value="foo">${regencies[i]}</option>`)
        }
    });
});
