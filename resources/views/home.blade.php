@inject('helper', 'App\Helpers\Helper')
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <form action="">
                <div class="form-row">
                    <div class="col">
                        <label for="type">Type</label>
                        <select name="type" id="type" class="form-control" onchange="this.form.submit()">
                            <option value="">All</option>
                            <option value="G" {{ $type == 'G' ? 'selected' : '' }}>{{ $helper->getUserType('G') }}</option>
                            <option value="R" {{ $type == 'R' ? 'selected' : '' }}>{{ $helper->getUserType('R') }}</option>
                        </select>
                    </div>

                    <div class="col">
                        <label for="province">Province</label>
                        <select name="province" id="province" class="form-control" onchange="this.form.submit()">
                            <option value="">All</option>
                            @foreach($items as $key=>$value)
                                <option value="{{ $key }}" data-regency="{{ implode($value, ',') }}" {{ $province == $key ? 'selected' : '' }}>{{ $key }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col">
                        <label for="regency">Regency</label>
                        <select name="regency" id="regency" class="form-control" {{ count($regencies) > 0 ?  '' : 'disabled' }}  onchange="this.form.submit()">
                            @if(count($regencies) > 0)
                                <option>Select regency</option>
                                @foreach($regencies as $item)
                                    <option value="{{ $item }}" {{ $item == $regency ? 'selected' : '' }}>{{ $item }}</option>
                                @endforeach
                            @else
                                <option>Select province first</option>
                            @endif
                        </select>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-12 mt-3">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Type</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Province</th>
                    <th scope="col">Regency</th>
                </tr>
                </thead>
                <tbody>

                @foreach($assistances as $assistance)
                    <tr>
                        <th scope="row">{{ $assistance->name }}</th>
                        <td class="text-capitalize">{{ $helper->getUserType($assistance->type) }}</td>
                        <td>{{ $assistance->mobile_phone }}</td>
                        <td>{{ $assistance->province }}</td>
                        <td>{{ $assistance->regency }}</td>
                    </tr>
                @endforeach

                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6">{{ $assistances->appends(['type' => $type, 'province' => $province, 'regency' => $regency])->links() }}</td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>
@endsection
