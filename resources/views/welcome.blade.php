<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!-- Include stylesheet -->
        <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

        <style>
            #map {
                height: 100%;
            }
            #description {
                font-family: Roboto;
                font-size: 15px;
                font-weight: 300;
            }

            #infowindow-content .title {
                font-weight: bold;
            }

            #infowindow-content {
                display: none;
            }

            #map #infowindow-content {
                display: inline;
            }

            .pac-card {
                margin: 10px 10px 0 0;
                border-radius: 2px 0 0 2px;
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                outline: none;
                box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
                background-color: #fff;
                font-family: Roboto;
            }

            #pac-container {
                padding-bottom: 12px;
                margin-right: 12px;
            }

            .pac-controls {
                display: inline-block;
                padding: 5px 11px;
            }

            .pac-controls label {
                font-family: Roboto;
                font-size: 13px;
                font-weight: 300;
            }

            #pac-input {
                background-color: #fff;
                font-family: Roboto;
                font-size: 15px;
                font-weight: 300;
                margin-left: 12px;
                padding: 0 11px 0 13px;
                text-overflow: ellipsis;
                width: 400px;
            }

            #pac-input:focus {
                border-color: #4d90fe;
            }

            #title {
                color: #fff;
                background-color: #4d90fe;
                font-size: 25px;
                font-weight: 500;
                padding: 6px 12px;
            }
        </style>
    </head>
    <body>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 mt-5">
                <form action="{{ route('storeAssistance') }}" method="POST" autocomplete="off">
                    @csrf

                    <div class="form-row">
                        <div class="col">
                            <label for="type">Type</label>
                            <select name="type" id="type" class="form-control" autofocus required>
                                <option value="G">Memberi bantuan</option>
                                <option value="R">Menerima bantuan</option>
                            </select>
                        </div>

                        <div class="col">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Gofar Hilman" required>
                        </div>
                    </div>

                    <div class="form-group mt-2">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" name="address" id="address" required>
                        <div id="map"></div>
                    </div>

                    <div class="form-row">
                        <div class="col">
                            <label for="province">Province</label>
                            <input type="text" class="form-control" name="province" id="province" placeholder="Bali">
                        </div>

                        <div class="col">
                            <label for="regency">Regency</label>
                            <input type="text" class="form-control" name="regency" id="regency" placeholder="Kota Denpasar">
                        </div>
                    </div>

                    <div class="form-row mt-2">
                        <div class="col">
                            <label for="district">District</label>
                            <input type="text" class="form-control" name="district" id="district" placeholder="Kecamatan Denpasar Timur">
                        </div>

                        <div class="col">
                            <label for="village">Village</label>
                            <input type="text" class="form-control" name="village" id="village" placeholder="Sumerta Kaja">
                        </div>
                    </div>

                    <div class="form-group mt-2">
                        <label for="mobile_phone">Phone</label>
                        <input type="text" class="form-control" name="mobile_phone" id="mobile_phone" placeholder="08212345678" required>
                    </div>

                    <div class="form-group">
                        <label for="needs">Needs</label>
                        <div id="editor"></div>
                    </div>
                    <input type="hidden" name="needs" id="needs">

                    <input type="hidden" name="lat" id="Latitude">
                    <input type="hidden" name="lng" id="Longitude">

                    <button class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>

    <script>
        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -8.1186575, lng: 115.0801284},
                zoom: 15
            });
            var card = document.getElementById('pac-card');
            var input = document.getElementById('address');

            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

            var autocomplete = new google.maps.places.Autocomplete(input);

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', map);

            // Set the data fields to return when the user selects a place.
            autocomplete.setFields(
                ['address_components', 'geometry', 'icon', 'name']);

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);
            var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
            });

            autocomplete.addListener('place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    // User entered the name of a Place that was not suggested and
                    // pressed the Enter key, or the Place Details request failed.
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                document.getElementById("Latitude").value = place.geometry.location.lng();
                document.getElementById("Longitude").value = place.geometry.location.lat();

                var address = '';
                if (place.address_components) {

                    console.log(place.address_components);

                    document.getElementById("province").value = place.address_components[4].long_name;
                    document.getElementById("regency").value = place.address_components[3].long_name;
                    document.getElementById("district").value = place.address_components[2].long_name;
                    document.getElementById("village").value = place.address_components[1].long_name;

                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindowContent.children['place-icon'].src = place.icon;
                infowindowContent.children['place-name'].textContent = place.name;
                infowindowContent.children['place-address'].textContent = address;
                infowindow.open(map, marker);
            });

        }
    </script>

    <!-- Scripts -->
    <script src="{{ asset('js/index.js') }}" defer></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqNDrJhXjrnDWLE3E4wd3kk3o0kXAjpfE&libraries=places&callback=initMap"
            async defer></script>
    <!-- Include the Quill library -->
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <script>
        var options = {
            placeholder: 'Saya memerlukan beras 1 karung untuk',
            theme: 'snow'
        };

        var quill = new Quill('#editor', options);
        var preciousContent = document.getElementById('needs');

        quill.on('text-change', function() {
            preciousContent.value = quill.root.innerHTML;
        });
    </script>

    </body>
</html>
